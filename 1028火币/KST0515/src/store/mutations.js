import * as types from './mutation-types'

export default {
  [types.SET_THEME](state, cls) {
    state.themeCls = cls
  },
  updateSocketData(state, val){
    state.socketData = val;
  },
  coinListdata(state, val){
    state.socketData = val;
  },
}

