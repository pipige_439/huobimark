require.config({
    paths: {
        "jquery": "../../static/kline2/jquery",
        "jquery.mousewheel": "../../static/kline2/jquery.mousewheel",
        "sockjs": "../../static/kline2/sockjs",
        "stomp": "../../static/kline2/stomp",
        "kline": "../../static/kline2/kline"
    },
    shim: {
        "jquery.mousewheel": {
            deps: ["jquery"]
        },
        "kline": {
            deps: ["jquery.mousewheel", "sockjs", "stomp"]
        }
    }
});	

var widt=$(window).width();
require(['kline'], function () {
    var kline = new Kline({
        element: "#kline_container",
        width: widt,
        height: 1000,
        theme: 'dark', // light/dark
        language: 'zh-cn', // zh-cn/en-us/zh-tw
        ranges: ["1w", "1d", "1h", "30m", "15m", "5m", "1m", "line"],
        //symbol: coinkline,
        symbol: "BNB_USDT",
        symbolName: "BTC/USDT",
        type: "poll", // poll/socket
       // url: "../kline2/kline/mock.json",
        url: "http://192.168.16.223/API/klines",
        limit: 20,
        reverseColor: false,
        intervalTime: 5000,
        debug: false,
        showTrade: false
    });
    kline.draw();
});	
window.onresize=function(){
	location.reload(); 	
			}