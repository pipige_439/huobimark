let apiCfg = (function () {
	return {
		// www.kst5g.com  192.168.124.223:8080
		// baseURL: process.env.NODE_ENV === 'production' ? "http://www.kst5g.com" : "/API",
		baseURL: process.env.NODE_ENV === 'production' ? "https://www.kst5g.com" : "https://www.kst5g.com",
		socketUrl: process.env.NODE_ENV === 'production' ? "wss://www.kst5g.com" : "wss://www.kst5g.com",
		klineSocketUrl: process.env.NODE_ENV === 'production' ? "wss://www.kst5g.com/ws/websocket/" : "wss://www.kst5g.com/ws/websocket/",
		// 二维码路径 邀请链接
		qrUrl: 'https://www.kst5g.com/',
		downAppurl:'https://www.kst5g.com/download/index.html',
		//获取app地址
		version: {
			url: "version"
		},
		//首页
		currencylist: {
			url: 'currency_list',
			method:'post',
			reqUrl: "/home/action",
		},
		assess: {
			url: 'assess',
			method:'post',
			reqUrl: "/home/action",
		},
		currency_introduction: {
			url: 'currency_introduction',
			method:'post',
			reqUrl: "/home/action",
		},
		symbols: {
			url: 'symbols',
			method:'post',
			reqUrl: "/home/action",
		},
		volume: {
			url: 'volume',
			method:'post',
			reqUrl: "/home/action",
		},
		overview: {
			url: 'overview',
			method:'post',
			reqUrl: "/home/action",
		},
		areacode: {
			url: "area_code",
			method:'post',
			reqUrl: "/home/action",
		},
		// 首页banner
		banner: {
			url: "banner",
			method:'post',
			reqUrl: "home/action"
		},
		articles: {
			url: "articles",
			method:'post',
			reqUrl: "home/action"
		},
		//个人中心、登录、注册、忘记密码
		getmyrecom: {
			url: "getmyrecom",
			reqUrl: "user/action",
		},
		getrecomcode: {
			url: "getrecomcode",
			reqUrl: "user/action",
		},
		updatetradpassword: {
			url: "updatetradpassword",
			reqUrl: "user/action",
		},
		updateloginpassword: {
			url: "updateloginpassword",
			reqUrl: "user/action",
		},
		updatetel: {
			url: "updatetel",
			reqUrl: "user/action"
		},
		updateemail: {
			url: 'updateemail',
			reqUrl: "user/action"
		},
		updaterealyname: {
			url: 'updaterealyname',
			reqUrl: "user/action"
		},
		updategoogle: {
			url: 'updategoogle',
			reqUrl: "user/action",
		},
		getgooglekey: {
			url: 'getgooglekey',
			reqUrl: "user/action",
		},
		getusersettinglog: {
			url: 'getusersettinglog',
			reqUrl: "user/action",
		},
		getuserloginlog: {
			url: 'getuserloginlog',
			method:'post',
			reqUrl: "user/action",
		},
		getuserinfo: {
			url: 'getuserinfo',
			reqUrl: "user/action",
		},
		forgetpwd: {
			url: 'forgetpwd',
			reqUrl: "user/action",
		},
		register: {
			url: "register",
			reqUrl: "user/action",
		},
		checkcode: {
			url: "checkcode",
			reqUrl: "user/action",
		},
		login: {
			url: 'login',
			reqUrl: "user/action",
		},
		getcode: {
			url: "code",
			reqUrl: "user/action",
		},
		// ----资产---
		c2cbalance: {
			url: "c2c_balance",
			reqUrl: "/assets/action",
		},
		tradbalance: {
			url: 'trad_balance',
			reqUrl: "/assets/action",
		},
		c2ctranferbalance: {
			url: 'c2c_tranfer_balance',
			reqUrl: "/assets/action",
		},
		c2ctranfer: {
			url: 'c2c_tranfer',
			reqUrl: "/assets/action",
		},
		c2cassetsamount: {
			url: 'c2c_assets_amount',
			reqUrl: "/assets/action",
		},
		c2cassets: {
			url: "c2c_assets",
			reqUrl: "/assets/action",
		},
		removetoaddress: {
			url: 'remove_toaddress',
			reqUrl: "/assets/action",
		},
		addtoaddress: {
			url: "add_toaddress",
			reqUrl: "/assets/action",
		},
		toaddress: {
			url: "toaddress",
			reqUrl: "/assets/action",
		},
		coins: {
			url: "coins",
			reqUrl: "/assets/action",
		},
		finance_type_coin: {
			url: "finance_type_coin",
			reqUrl: "/assets/action",
		},
		finance_type: {
			url: "finance_type",
			reqUrl: "/assets/action",
		},
		finance_coin: {
			url: "finance_coin",
			reqUrl: "/assets/action",
		},
		finance_list: {
			url: "finance_list",
			reqUrl: "/assets/action",
		},
		outcoin: {
			url: "outcoin",
			reqUrl: "/assets/action",
		},
		withdrawal_tips: {
			url: "withdrawal_tips",
			reqUrl: "/assets/action",
		},
		recharge_tips: {
			url: "recharge_tips",
			reqUrl: "/assets/action",
		},
		getaddress: {
			url: "address",
			reqUrl: "/assets/action",
		},
		trad_assets_amount: {
			url: 'trad_assets_amount',
			method:'post',
			reqUrl: "/assets/action",
		},
		trad_assets: {
			url: 'trad_assets',
			method:'post',
			reqUrl: "/assets/action",
		},
		// end-=---------------
		

		// 订单
		c2c_order: {
			url: 'c2c_order',
			method:'post',
			reqUrl: "/order/action",
		},
		c2c_entrust: {
			url: 'c2c_entrust',
			method:'post',
			reqUrl: "/order/action",
		},
		trad_entrust: {
			url: 'trad_entrust',
			method:'post',
			reqUrl: "/order/action",
		},
		trad_history: {
			url: 'trad_history',
			method:'post',
			reqUrl: "/order/action",
		},
		trad_order: {
			url: 'trad_order',
			method:'post',
			reqUrl: "/order/action",
		},
		// 币币交易
		getkline: {
			url: "kline",
			method:'post',
			reqUrl: "/trad/kline",
		},
		sell_market: {
			url: "sell_market",
			method:'post',
			reqUrl: "/trad/action",
		},
		buy_market: {
			url: "buy_market",
			method:'post',
			reqUrl: "/trad/action",
		},
		sell_limit: {
			url: "sell_limit",
			method:'post',
			reqUrl: "/trad/action",
		},
		buy_limit: {
			url: "buy_limit",
			method:'post',
			reqUrl: "/trad/action",
		},
		entrust_history: {
			url: "entrust_history",
			method:'post',
			reqUrl: "/trad/action",
		},
		entrust_order: {
			url: "entrust_order",
			method:'post',
			reqUrl: "/trad/action",
		},
		top_order: {
			url: "top_order",
			method:'post',
			reqUrl: "/trad/action",
		},
		entrust: {
			url: "entrust",
			method:'post',
			reqUrl: "/trad/action",
		},
		info: {
			url: "info",
			method:'post',
			reqUrl: "/trad/action",
		},
		close_entrust: {
			url: "close_entrust",
			method:'post',
			reqUrl: "/trad/action",
		},
		
		// 上传图片
		upimg: {
			url: "upimg",
			method:'post',
			reqUrl: "/user/img",
		},
		// C2C交易
		closec2corder: {
			url: "closec2corder",
			method:'post',
			reqUrl: "/c2c/action",
		},
		fastsellcoin: {
			url: "fastsellcoin",
			method:'post',
			reqUrl: "/c2c/action",
		},
		fastbuycoin: {
			url: "fastbuycoin",
			method:'post',
			reqUrl: "/c2c/action",
		},
		checkusermerchant: {
			url: "checkusermerchant",
			method:'post',
			reqUrl: "/c2c/action",
		},
		checkpaytype: {
			url: "checkpaytype",
			method:'post',
			reqUrl: "/c2c/action",
		},
		questc2corder: {
			url: "questc2corder",
			method:'post',
			reqUrl: "/c2c/action",
		},
		getmypaytype:{
			url: 'getmypaytype',
			method:'post',
			reqUrl: "/c2c/action",
		},
		// 收款方式
		updatewx: {
			url: "updatewx",
			method:'post',
			reqUrl: "/c2c/action",
		},
		updatebankcard: {
			url: "updatebankcard",
			method:'post',
			reqUrl: "/c2c/action",
		},
		updatezfb: {
			url: "updatezfb",
			method:'post',
			reqUrl: "/c2c/action",
		},
		addorderinfo: {
			url: "addorderinfo",
			method:'post',
			reqUrl: "/c2c/action",
		},
		getorderinfo: {
			url: "getorderinfo",
			method:'post',
			reqUrl: "/c2c/action",
		},
		getmoneyok: {
			url: "getmoneyok",
			method:'post',
			reqUrl: "/c2c/action",
		},
		sendmoneyok: {
			url: "sendmoneyok",
			method:'post',
			reqUrl: "/c2c/action",
		},
		sellc2cmarket: {
			url: "sellc2cmarket",
			method:'post',
			reqUrl: "/c2c/action",
		},
		buyc2cmarket: {
			url: "buyc2cmarket",
			method:'post',
			reqUrl: "/c2c/action",
		},
		updatec2ctrad: {
			url: "updatec2ctrad",
			method:'post',
			reqUrl: "/c2c/action",
		},
		getmyc2cmarketorder: {
			url: "getmyc2cmarketorder",
			method:'post',
			reqUrl: "/c2c/action",
		},
		addc2centrust: {
			url: "getmyc2cmarketorder",
			method:'post',
			reqUrl: "/c2c/action",
		},
		getc2cmarketorder: {
			url: "getc2cmarketorder",
			method:'post',
			reqUrl: "/c2c/action",
		},
		getc2ccoin: {
			url: "getc2ccoin",
			method:'post',
			reqUrl: "/c2c/action",
		},
		addmerchant: {
			url: "addmerchant",
			method:'post',
			reqUrl: "/c2c/action",
		},
		getmerchantservices: {
			url: "getmerchantservices",
			method:'post',
			reqUrl: "/c2c/action",
		},
		// 上币申请
		add_apply: {
			url: "add_apply",
			method:'post',
			reqUrl: "/apply/action",
		},
	}
}());


export default apiCfg;