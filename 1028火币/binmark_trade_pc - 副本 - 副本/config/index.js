'use strict'
const path = require('path')
// 
// http://47.111.77.206:8084
// https://www.binmark.io
// http://192.168.16.51:80
module.exports = {
  dev: {
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    proxyTable: {
      '/API': {
        target: "https://www.binmark.co",
        changeOrigin: true
      }
    },
    host: "192.168.16.103",
    port: 8013,
    autoOpenBrowser: true,
    errorOverlay: true,
    notifyOnErrors: true,
    poll: false,
    useEslint: true,
    showEslintErrorsInOverlay: false,
    devtool: 'cheap-module-eval-source-map',
    cacheBusting: true,
    cssSourceMap: true
  },
  build: {
    index: path.resolve(__dirname, '../mscoin/index.html'),
    assetsRoot: path.resolve(__dirname, '../mscoin'),
    assetsSubDirectory: 'pcstatic',
    assetsPublicPath: './',
    productionSourceMap: false,
    devtool: '#source-map',
    productionGzip: false,
    productionGzipExtensions: ['js', 'css'],
    bundleAnalyzerReport: process.env.npm_config_report
  }
}
