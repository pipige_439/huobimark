// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import Vue from 'vue'
import router from './router'
import store from "./store/index";
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import MyHeader from "@/components/header";

import { Picker  } from 'mint-ui';
Vue.component(Picker.name, Picker)

//按需引入vant
import { Tabbar, TabbarItem ,Swipe, SwipeItem, Popup,Button, Cell, CellGroup ,Icon,List,Row, Col ,Actionsheet ,Dialog, 
  Tab, Tabs,Field ,Search,NumberKeyboard,PasswordInput,RadioGroup, Radio} from 'vant';

Vue.use(Tabbar).use(TabbarItem).use(Swipe).use(SwipeItem).use(Popup).use(Button).use(Cell).use(CellGroup).use(Icon).use(Row).use(Col).use(List)
.use(Tab).use(Tabs).use(Field).use(Actionsheet ).use(Dialog).use(NumberKeyboard).use(PasswordInput).use(Search).use(RadioGroup).use(Radio)

Vue.use(ElementUI);

import App from "./App";
import httpServer from "./http/axios"; //封装ajax请求
import Api from "./http/api"; //接口api
import validator from "./store/validator";
import $ from "jquery";

// import VueClipboard from 'vue-clipboard2'
// Vue.use(VueClipboard)

Vue.use(MyHeader);
Vue.component('my-header', MyHeader);
import filter from '@/store/filter';
Object.keys(filter).forEach(key => {
  Vue.filter(key, filter[key])
});

import '../src/assets/css/common.css'
import '../src/assets/css/library.less'
import '../src/assets/css/vant.less'

Vue.prototype.$ajax = httpServer;
Vue.prototype.$api = Api;
Vue.prototype.$validator = validator;
Vue.prototype.$$ = $;

Vue.prototype.goBack = () => {
  router.go(-1);
}
Vue.prototype.pushPath = (path) => {
  router.push(path);
}


Vue.config.productionTip = false;

router.beforeEach((to, from, next) => {
  sessionStorage.setItem('lastPage',from.name)
  //检查token有没有
  if (to.meta.requiresAuth) {
    if (localStorage.getItem("token")) {
      next();
    } else {
      next({
        path: "/login",
      }); 
    }
  } else {
    next();
    store.commit('updateLoginState',false)
  }
});

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
