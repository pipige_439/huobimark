import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router);
const routes = [
  {
    path: '/',
    name: 'Main',
    component: r => require(['@/view/Main'], r),
    meta: { auth: false, title: "测试测试-区块链数字资产交易平台" }
  },
  {
    path: '/user',
    name: 'user',
    component: r => require(['@/view/user/User'], r),
    redirect: "/user/login",
    children: [
      {
        path: '/user/login',
        name: 'Login',
        component: r => require(['@/view/user/Login'], r),
        meta: { auth: false, title: "用户登录" }
      },
      {
        path: '/user/register',
        name: 'register',
        component: r => require(['@/view/user/Register'], r),
        meta: { auth: false, title: "用户注册" }
      },
      {
        path: '/user/resetpwd',
        name: 'resetpwd',
        component: r => require(['@/view/user/resetPassword'], r),
        meta: { auth: false, title: "重置密码" }
      }
    ]
  },
  {
    path: '/account',
    component: r => require(['@/view/account/Main'], r),
    redirect: '/account/security',
    children: [
      {
        path: '/account/security',
        name: 'Security2',
        component: r => require(['@/view/account/Security'], r),
        meta: { auth: true, title: "账户安全" }
      },
      {
        path: '/account/identify',
        name: 'Identify2',
        component: r => require(['@/view/account/Identify'], r),
        meta: { auth: true, title: "身份认证" }
      }, {
        path: "/account/work_order",
        name: "workOrder",
        component: r => require(['@/view/account/workOrder'], r),
        meta: { auth: true, title: "提交工单" }
      }
    ]
  },
  // 个人中心
  {
    path: '/mycenter',
    redirect: '/mycenter/security',
    component: r => require(['@/view/mycenter/Main'], r),
    children: [
      {
        path: '/mycenter/security',
        name: 'security',
        component: r => require(['@/view/mycenter/security'], r),
        meta: { auth: true, title: "账户安全-安全中心-个人中心" }
      },
      {
        path: '/mycenter/safetyrecord',
        name: 'safetyrecord',
        component: r => require(['@/view/mycenter/safetyrecord'], r),
        meta: { auth: true, title: "安全记录-账户安全-安全中心" }
      },
      {
        path: '/mycenter/bindga',
        name: 'bindga',
        component: r => require(['@/view/mycenter/bindga'], r),
        meta: { auth: true, title: "绑定谷歌验证器-安全中心" }
      },
      {
        path: '/mycenter/uc_auth',
        name: 'uc_auth',
        component: r => require(['@/view/mycenter/uc_auth'], r),
        meta: { auth: true, title: "身份认证-安全中心" }
      },
      {
        path: '/mycenter/invite',
        name: 'invite',
        component: r => require(['@/view/mycenter/invite'], r),
        meta: { auth: true, title: "邀请好友-个人中心" }
      },
      {
        path: '/mycenter/finance',
        name: 'finance',
        component: r => require(['@/view/mycenter/finance'], r),
        meta: { auth: true, title: "币币账户-充币-提币" }
      },
      {
        path: '/mycenter/letradeSet',
        name: 'letradeSet',
        component: r => require(['@/view/mycenter/letradeSet'], r),
        meta: { auth: true, title: "法币设置-账户设置-个人中心" }
      },
      {
        path:'/mycenter/transac',
        name: 'transac',
        component: r => require(['@/view/mycenter/transac'], r),
        meta: { auth: true, title: "币币交易-委托订单" }
      },
      {
        path: '/personalCenter/entrustment',
        name: 'Entrustment',
        component: r => require(['@/view/personalCenter/Entrustment'], r),
        meta: { auth: true, title: "我的委托" }
      },
      {
        path: '/personalCenter/deal_history',
        name: 'dealHistory',
        component: r => require(['@/view/personalCenter/dealHistory'], r),
        meta: { auth: true, title: "交易历史" }
      },
      {
        path: '/personalCenter/otc_history',
        name: 'otcHistory',
        component: r => require(['@/view/personalCenter/otcHistory'], r),
        meta: { auth: true, title: "OTC记录" }
      },
      {
        path: '/personalCenter/c2c_history',
        name: 'c2cHistory',
        component: r => require(['@/view/personalCenter/c2cHistory'], r),
        meta: { auth: true, title: "C2C记录" }
      },
      {
        path: '/personalCenter/vip_detail',
        name: 'VipDetail',
        component: r => require(['@/view/personalCenter/VipDetail'], r),
        meta: { auth: true, title: "vip说明" }
      },
      {
        path: '/personalCenter/myFlink',
        name: 'myFlink',
        component: r => require(['@/view/personalCenter/myFlink'], r),
        meta: { auth: true, title: "我的推广链接" }
      },
      // arpointBuyList   arPointuseList
      {
        path: '/personalCenter/award',
        name: 'award',
        component: r => require(['@/view/personalCenter/award'], r),
        meta: { auth: false, title: "返佣奖励" }
      },
      {
        path: '/personalCenter/apikeys',
        name: 'apkkeys',
        component: r => require(['@/view/personalCenter/apikeys'], r),
        meta: { auth: false, title: "API Keys" }
      },
    ]
  },
  {
    path: '/currency_trade',
    name: 'currencyTrade',
    component: r => require(['@/view/trade/currencyTrade'], r),
    meta: { auth: false, title: "币币交易" }
  },
  // {
  //   path: '/c2c_index',
  //   name: 'C2Cindex',
  //   component: r => require(['@/view/trade/C2Cindex'], r),
  //   meta: { auth: false, title: "C2C交易" }
  // },
  // {
  //   path: '/c2c_trade',
  //   name: 'c2cTrade',
  //   component: r => require(['@/view/trade/C2CTrade'], r),
  //   meta: { auth: false, title: "C2C交易" }
  // },
  
  // {
  //   path: '/C2CTrade_list',
  //   name: 'C2CTrade_list',
  //   component: r => require(['@/view/trade/C2CTrade_list'], r),
  //   meta: { auth: true, title: "C2C交易列表" }
  // },
  // {
  //   path: '/otc_trade',
  //   name: 'otcTrade',
  //   component: r => require(['@/view/trade/OTCTrade'], r),
  //   meta: { auth: false, title: "OTC交易" }
  // },
  // {
  //   path: '/otc_tradelist',
  //   name: 'otcTradeList',
  //   component: r => require(['@/view/trade/OTCTrade_list'], r),
  //   meta: {auth: true, title: "交易记录" }
  // },
  // {
  //   path:'/tradeSuss',
  //   name: 'tradeSuss',
  //   component: r => require(['@/view/trade/tradeSuss'], r),
  //   meta: { auth: true, title: "交易成功....即将跳转" }
  // },
  // {
  //   path:'/order_info',
  //   name: 'orderInfo',
  //   component: r => require(['@/view/trade/orderInfo'], r),
  //   meta: { auth: true, title: "交易记录详情" }
  // },
  {
    path: '/notice',
    component: r => require(['@/view/notices/Notice'], r),
    children: [
      {
        path: '/',
        name: 'noticeList',
        component: r => require(['@/view/notices/noticeList'], r),
        meta: { auth: false, title: "系统公告" },
      },
      {
        path: '/notice/detail/',
        name: 'detail',
        component: r => require(['@/view/notices/noticeDetail'], r),
        meta: { auth: false, title: "系统公告详情" }
      }
    ]
  },
  {
    path: '/help_center',
    component: r => require(['@/view/helpCenter/helpMain'], r),
    redirect: '/helpcenter/helpMain',
    children: [
      {
        path: '/help_center/h_home',
        name: 'helpmain',
        component: r => require(['@/view/helpCenter/h_home'], r),
        meta: { title: "帮助中心-首页" }
      },
      {
        path: '/help_center/currency_intro',
        name: 'currencyIntro',
        component: r => require(['@/view/helpCenter/currencyIntro'], r),
        meta: { title: "帮助中心-币种介绍" }
      },
      {
        path: '/help_center/faq',
        name: 'FAQ',
        component: r => require(['@/view/helpCenter/Faq'], r),
        meta: { title: "帮助中心-问题中心" }
      },
      {
        path: '/help_center/faq_detail',
        name: 'faqDetail',
        component: r => require(['@/view/helpCenter/faqDetail'], r),
        meta: { title: "帮助中心-问题中心" }
      },
      {
        path: '/help_center/law',
        name: "Law",
        component: r => require(['@/view/helpCenter/Law'], r),
        meta: { title: "帮助中心-法律声明" }
      },
      {
        path: '/help_center/privacy',
        name: "Privacy",
        component: r => require(['@/view/helpCenter/privacy'], r),
        meta: { title: "帮助中心-隐私条款" }
      },
      {
        path: '/help_center/rules',
        component: r => require(['@/view/helpCenter/Rules'], r),
        name: "Rules",
        meta: { title: "帮助中心-交易须知" }
      },
      {
        path: '/help_center/risk',
        component: r => require(['@/view/helpCenter/Risk'], r),
        name: "Risk",
        meta: { title: "帮助中心-风险声明" }
      },
      {
        path: '/help_center/about',
        component: r => require(['@/view/helpCenter/About'], r),
        name: "About",
        meta: { title: "帮助中心-企业优势" }
      },
      {
        path: '/help_center/contact',
        component: r => require(['@/view/helpCenter/Contact'], r),
        name: "Contact",
        meta: { title: "帮助中心-联系我们" }
      },
      {
        path: '/upcoinInfo',
        name: 'upcoinInfo',
        component: r => require(['@/view/helpCenter/upcoinInfo'], r),
        meta: { auth: false, title: "上币申请" }
      },
      {
        path: '/upcoinForm',
        name: 'upcoinForm',
        component: r => require(['@/view/helpCenter/upcoinForm'], r),
        meta: { auth: false, title: "上币申请" }
      },
    ]
  },
  {
    path: "/kline_trade",
    name: "klineTrade",
    component: r => require(['@/view/trade/klineTrade'], r),
    meta: { auth: false, title: 'K线交易' }
  }, {
    path: "/trade/auto",
    name: "Auto",
    component: r => require(['@/view/other/currencyTradeAuto'], r),
    meta: {
      auth: true,
      title: "自动下单"
    }
  },
  {
    // ar 点卡功能 by 0517
    path: '/Startup/arTimeca',
    name: 'arTimeca',
    component: r => require(['@/view/Startup/arTimeca'], r),
    meta: { auth: false, title: "点卡套餐" }
  },
  {
    path: '/coinPrice',
    name: 'coinPrice',
    component: r => require(['@/view/coinPrice/coinPrice'], r),
    meta: { auth: false, title: "币种行情" }
  },
  //新版 法币交易
  {
    path: '/FrTrade',
    component: r => require(['@/view/FrTrade/frtradeMain'], r),
    redirect: '/FrTrade/c2cTrade',
    children: [
      {
        path: '/FrTrade/c2cTrade',
        name: 'c2cTrade',
        component: r => require(['@/view/FrTrade/c2cTrade'], r),
        meta: { title: "C2C交易-买入-法币交易" }
      },
      {
        path: '/FrTrade/C2Cindexsell',
        name: 'C2Cindexsell',
        component: r => require(['@/view/FrTrade/C2Cindexsell'], r),
        meta: { auth: false, title: "C2C交易卖出-法币交易" }
      },
      {
        path: '/FrTrade/otcTrade',
        name: 'otcTrade',
        component: r => require(['@/view/FrTrade/otcTrade'], r),
        meta: { auth: false,title: "OTC交易-法币交易" }
      },
      {
        path: '/FrTrade/business',
        name: 'business',
        component: r => require(['@/view/FrTrade/business'], r),
        meta: { auth: false,title: "广告权限申请-CTC交易-法币交易" }
      },
      {
        path: '/FrTrade/applyform',
        name: 'applyform',
        component: r => require(['@/view/FrTrade/applyform'], r),
        meta: {auth: true, title: "广告权限申请-CTC交易-法币交易" }
      },
      {
        path: '/FrTrade/C2CTrade_list',
        name: 'C2CTrade_list',
        component: r => require(['@/view/FrTrade/C2CTrade_list'], r),
        meta: { auth: true,title: "C2C记录-法币订单" }
      },
      {
        path:'/FrTrade/order_info',
        name: 'orderInfo',
        component: r => require(['@/view/FrTrade/orderInfo'], r),
        meta: { auth: true, title: "交易记录详情" }
      },
      {
        path:'/FrTrade/postshop',
        name: 'postshop',
        component: r => require(['@/view/FrTrade/postshop'], r),
        meta: { auth: true, title: "商户发布广告" }
      },
      {
        path:'/FrTrade/shoporder',
        name: 'shoporder',
        component: r => require(['@/view/FrTrade/shoporder'], r),
        meta: { auth: true, title: "商户广告列表-商户发布广告" }
      },
      
    ]
  },
  {
    path: '/property',
    component: r => require(['@/view/property/Main'], r),
    redirect: 'Property',
    children: [
      {
        path: '/',
        name: 'Property',
        component: r => require(['@/view/property/Property'], r),
        meta: { auth: true, title: "我的资产" }
      },
      {
        path: '/property/address_admin',
        name: 'addressAdmin',
        component: r => require(['@/view/property/addressAdmin'], r),
        meta: { auth: true, title: "添加地址" }
      },
      {
        path: '/property/property_record',
        name: 'propertyRecord',
        component: r => require(['@/view/property/propertyRecord'], r),
        meta: { auth: true, title: "财务记录" }
      },
      {
        path: '/property/rechargeRecord',
        name: 'rechargeRecord',
        component: r => require(['@/view/property/rechargeRecord'], r),
        meta: { auth: true, title: "充值提币记录" }
      },
      // 66
      {
        path: '/property/c2cbalances',
        name: 'c2cbalances',
        component: r => require(['@/view/property/c2cbalances'], r),
        meta: { auth: true, title: "C2C账户资产-法币资产" }
      },
      {
        path: '/property/frbalances',
        name: 'frbalances',
        component: r => require(['@/view/property/frbalances'], r),
        meta: { auth: true, title: "合约资产" }
      },
      
    ]
  },  
]

export default new Router({ routes });
